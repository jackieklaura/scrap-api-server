Config = {
    "db" : {
        # Put the access data for the MariaDB database here.
        "host": "localhost",
        "user": "scrap",
        "pass": "PASSWORDGOESHERE",
        "database": "scrap",
    },
    'meta' : {
        # Provide an e-mail address or other contact information (used mostly
        # in internel server error respsonses)
        'admin_contact': 'admin@example.com'
    },
    'uploads' : {
        # Locations for the final scan uploads and the temporary upload files.
        # Use relative paths only if under the flask app root
        'folder': 'uploads',
        'temp_folder': 'uploads_temp',
        # Maximum file upload size (in bytes) for public users
        'public_size_limit': 10 * 1024,
        # General maximum upload size (in bytes) the Flask app should handle
        'size_limit': 23 * 1024 * 1024,
        # The allowed extensions for file uploads.
        # Currently only tgz is support as an archive file. For other types
        # (even if it is only .tar.gz), the POST method of the
        # resourcs.scan.ListOfScans class has to be adapted.
        'allowed_types': {'php', 'tgz'}
    },
    'auth' : {
        # The user name of the public user (make sure to have a corresponding
        # entry in the apikeys table of the database)
        'public_user': 'public',
        # Set this to True if public users can delete any public scan
        'public_user_can_delete': True,
        # If you set the above to False, make sure to provide some regular
        # cleanup schedule e.g. with a cron job and describe it here, like:
        # 'every full hour'
        # 'at midnight'
        # 'a day after a scans is created'
        'public_user_cleanup_schedule': 'every full hour',
    }
}
