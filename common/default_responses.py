from config import Config as cfg

NotAuthorized = {
    'error': {
        'code': 401,
        'message': 'You are not authorized to access this resource',
    }
}, 401

NoPublicDelete = {
    'error': {
        'code': 401,
        'message': 'Public scans cannot be deleted on this server.' \
            if not cfg['auth']['public_user_cleanup_schedule'] else \
            'Public scans are deleted by the server ' + cfg['auth']['public_user_cleanup_schedule']
    }
}, 401

InvalidUuid = {
    'error': {
        'code': 400,
        'message': 'The provided UUID is not valid!',
    }
}, 400

InvalidScanner = {
    'error': {
        'code': 400,
        'message': 'The requested scanner is not available',
    }
}, 400

UseMultipart = {
    'error': {
        'code': 400,
        'message': 'The provided media type is not valid',
        'additionalInfo': 'Use "multipart/form-data" for uploading files to scan'
    }
}, 400

FileNeeded = {
    'error': {
        'code': 400,
        'message': 'A file named "file" is mandatory for a scan upload',
    }
}, 400

NoEmptyFiles = {
    'error': {
        'code': 400,
        'message': 'Your uploaded file has 0 size. Go seek for emptiness somewhere else!',
    }
}, 400

InvalidArchiveContent = {
    'error': {
        'code': 400,
        'message': 'The uploaded archive contains invalid content',
        'additionalInfo': 'Archive members are not allowed to start with ' + \
            ' "../", "./" or "/" and there has to be at least one .php file'
    }
}, 400

FileTooBig = {
    'error': {
        'code': 413,
        'message': 'The uploaded file is too big.',
        'additionalInfo': 'Public users have a limit of ' + \
            str(cfg['uploads']['public_size_limit']) + ' bytes for scan uploads.'
    }
}, 413

WrongFileType = {
    'error': {
        'code': 415,
        'message': 'The file type you provided is not valid.',
        'additionalInfo': 'User either .pdf or .tgz files.'
    }
}, 415

ScanNotFound = {
    'error': {
        'code': 404,
        'message': 'There is no scan with this UUID',
    }
}, 404

FileNotFound = {
    'error': {
        'code': 404,
        'message': 'The requested file does not exist',
    }
}, 404

ExplanationNotFound = {
    'error': {
        'code': 404,
        'message': 'The requested explanation does not exist',
    }
}, 404

ExplanationParseError = {
    'error': {
        'code': 500,
        'message': 'The requested explanation could not be parsed',
        'additionalInfo': 'Please inform the server admin about this: ' + \
            cfg['meta']['admin_contact']
    }
}, 500

ExplanationParamMissing = {
    'error': {
        'code': 500,
        'message': 'The requested explanation is missing a required parameter',
        'additionalInfo': 'Please inform the server admin about this: ' + \
            cfg['meta']['admin_contact']
    }
}, 500

InternalServerError = {
    'error': {
        'code': 500,
        'message': 'There was an unspecified error processing your request.',
        'additionalInfo': 'Please inform the server admin about this and ' + \
            'provide the date and time of your request: ' + \
            cfg['meta']['admin_contact']
    }
}, 500
