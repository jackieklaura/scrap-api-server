import re

def isUuid(uuid=''):
    if re.match(r'^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$', str(uuid)):
        return True
    return False

def uuid(uuid=''):
    if isUuid(uuid):
        return str(uuid)
    return ''
