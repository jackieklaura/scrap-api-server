import db
from flask import current_app

def isUser(headers):
    apikey = headers.get('X-API-KEY')
    apiuser = headers.get('X-API-USER')
    sql = 'SELECT * FROM apikeys WHERE id = %s AND user = %s'
    rows = db.query(sql, apikey, apiuser)
    if rows:
        current_app.logger.debug('Authenticated user: ' + str(rows[0][1]))
        return True
    current_app.logger.debug('Not authenticated: ' + str(apiuser))
    return False
