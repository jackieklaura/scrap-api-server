import mysql.connector
from flask import current_app, g
from config import Config as cfg
from uuid import uuid4
from common import sanitize
import mimetypes
import shutil
import json
import os


def get_db():
    if 'db' not in g:
        g.db = mysql.connector.connect(
            host = cfg['db']['host'],
            user = cfg['db']['user'],
            passwd = cfg['db']['pass'],
            database = cfg['db']['database'],
        )
    return g.db

def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

def init_app(app):
    app.teardown_appcontext(close_db)


def query(q, *args):
    """Make a generic query against the database and return all result rows."""
    d = get_db()
    c = d.cursor()
    c.execute(q, args)
    rows = c.fetchall()
    return rows

def queryScans (apikey, limit=None, offset=None):
    """Query for all scan UUIDs related to an API key and return the items list."""
    sql = 'SELECT bin_to_uuid(uuid) FROM scans AS s ' + \
            'JOIN rel_apikeys_scans AS r ON s.id = r.scan ' +\
            'WHERE apikey = %s'
    if limit:
        sql += ' LIMIT ' + int(limit)
        if offset:
            sql += ' OFFSET ' + int(offset)
    rows = query(sql, apikey)
    items = []
    for row in rows:
        items.append(row[0])
    return items

def getScan (id, apikey):
    """Retrieve a single scan (if API key fits) and return the response dict"""
    sql = 'SELECT bin_to_uuid(uuid), stage, percentage, issuesFound, files,' + \
            'created, analysed FROM scans AS s JOIN rel_apikeys_scans AS r ' + \
            'ON s.id = r.scan WHERE apikey = %s AND '
    if isinstance(id, int):
        sql += 'id = ' + str(id)
    elif sanitize.isUuid(id):
        sql += 'uuid = uuid_to_bin("' + sanitize.uuid(id) + '")'
    else:
        return False

    d = get_db()
    c = d.cursor()
    c.execute(sql, (apikey,))
    scan = c.fetchone()

    if scan == None:
        return None
    return {
        'id': scan[0],
        'status': {
            'stage': scan[1],
            'percentage': scan[2],
        },
        'issuesFound': scan[3],
        'files': scan[4],
        'created': scan[5].isoformat() if scan[5] else None,
        'analysed': scan[6].isoformat() if scan[6] else None,
    }

def deleteScan (uuid, apikey):
    """Delete a scan and return True on success, or False if no scan was found"""
    sql = 'SELECT id FROM scans AS s JOIN rel_apikeys_scans AS r ON s.id = r.scan ' + \
            'WHERE apikey = %s AND uuid = uuid_to_bin(%s)'
    d = get_db()
    c = d.cursor()
    c.execute(sql, (apikey, uuid))
    row = c.fetchone()
    if not row:
        return False
    id = row[0]
    sql = 'DELETE FROM rel_apikeys_scans WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM rel_issues_files WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM issues WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM files WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM scans WHERE id = %s'
    c.execute(sql, (id,))
    d.commit()
    shutil.rmtree(os.path.join(current_app.config['UPLOADS']['folder'], uuid))
    return True

def submitScan (apikey, stage=None, percentage=None, issuesFound=None, files=None):
    sql = 'INSERT INTO scans VALUES (NULL, uuid_to_bin(%(id)s), ' + \
                '%(stage)s, %(percentage)s, %(issuesFound)s, %(files)s, ' + \
                'NOW(), NULL)'
    values = {
        'id' : str(uuid4()),
        'stage' : stage if stage else 'pending',
        'percentage' : percentage if percentage else 0,
        'issuesFound': issuesFound if issuesFound else 0,
        'files' : files if files else 0,
    }
    d = get_db()
    c = d.cursor()
    c.execute(sql, values)
    scanid = c.lastrowid
    sql = 'INSERT INTO rel_apikeys_scans VALUES (%s, %s)'
    c.execute(sql, (apikey, scanid))
    d.commit()
    scan = getScan(scanid, apikey)
    return scan

def populateFiles (scan_uuid):
    sql = 'SELECT id FROM scans WHERE uuid = uuid_to_bin(%s)'
    rows = query(sql, scan_uuid)
    if not rows:
        current_app.logger.error('populateFiles was called for a scan that does not exist:')
        current_app.logger.error('    uuid: ' + str(scan_uuid))
        return False
    id = rows[0][0]

    dirname = os.path.join(current_app.config['UPLOADS']['folder'], str(scan_uuid))
    if not os.path.isdir(dirname):
        current_app.logger.error('populateFiles could not find the upload folder for this scan')
        current_app.logger.error('    uuid: ' + str(scan_uuid))
        return False

    walker = os.walk(dirname)
    files = []
    count = 0
    for dir in walker:
        for file in dir[2]:
            if dir[0] == dirname:
                name = file
            else:
                name = dir[0][len(dirname)+1:] + '/' + file
            contentType = mimetypes.guess_type(dirname+'/'+file)[0]
            if contentType == None:
                if file.endswith('.php'):
                    contentType = 'application/x-php'
                else:
                    contentType = 'application/octet-stream'
            size = os.path.getsize(dir[0]+'/'+file)
            files.append((count, name, contentType, size))
            count += 1

    sql = 'INSERT INTO files VALUES ('+str(id)+', %s, %s, %s, %s)'
    d = get_db()
    c = d.cursor()
    c.executemany(sql, files)
    d.commit()
    return True

def get_file_count (scan_uuid):
    sql = 'SELECT id FROM scans WHERE uuid = uuid_to_bin(%s)'
    rows = query(sql, scan_uuid)
    if not rows:
        current_app.logger.error('get_file_count was called for a scan that does not exist:')
        current_app.logger.error('    uuid: ' + str(scan_uuid))
        return False
    id = rows[0][0]
    sql = 'SELECT COUNT(*) FROM files WHERE scan = %s'
    rows = query(sql, id)
    return rows[0][0]

def get_file_id (scan_id, path):
    sql = 'SELECT id FROM files WHERE scan = %s AND path = %s'
    rows = query(sql, scan_id, path)
    if rows:
        return rows[0][0]
    return None

def update_scan (scan_uuid, stage=None, percentage=None, issuesFound=None, files=None, analysed=None):
    sql = 'SELECT id FROM scans WHERE uuid = uuid_to_bin(%s)'
    rows = query(sql, scan_uuid)
    if not rows:
        current_app.logger.error('update_scan was called for a scan that does not exist:')
        current_app.logger.error('    uuid: ' + str(scan_uuid))
        return False
    id = rows[0][0]

    values = []
    sql = 'UPDATE scans SET '
    if stage:
        sql += 'stage = %s '
        values.append(stage)
    if percentage:
        if values:
            sql += ', '
        sql += 'percentage = %s '
        values.append(percentage)
    if issuesFound:
        if values:
            sql += ', '
        sql += 'issuesFound = %s '
        values.append(issuesFound)
    if files:
        if values:
            sql += ', '
        sql += 'files = %s '
        values.append(files)
    if analysed:
        if values:
            sql += ', '
        if analysed == "now":
            sql += 'analysed = NOW() '
        else:
            sql += 'analysed = %s '
            values.append(analysed)
    if not values:
        return False
    sql += 'WHERE id = %s'
    values.append(id)
    d = get_db()
    c = d.cursor()
    c.execute(sql, tuple(values))
    d.commit()
    return True

def submit_issues (scan_uuid, issues):
    sql = 'SELECT id FROM scans WHERE uuid = uuid_to_bin(%s)'
    rows = query(sql, scan_uuid)
    if not rows:
        current_app.logger.error('submit_issues was called for a scan that does not exist:')
        current_app.logger.error('    uuid: ' + str(scan_uuid))
        return False
    scan_id = rows[0][0]

    d = get_db()
    c = d.cursor()
    sql = 'SELECT MAX(id) FROM issues WHERE scan = %s'
    c.execute(sql, (scan_id,))
    row = c.fetchone()
    if row[0] == None:
        count = 0
    else:
        count = row[0] + 1
    for issue in issues:
        # issue columns: scan, id, scanner, rule, info, cli, type, explanation
        sql = 'INSERT INTO issues VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
        c.execute(sql, (
            scan_id, count, issue['source']['scanner'][:64], issue['source']['rule'], \
            issue['source']['info'], issue['source']['cli'], \
            issue['type'][:64], issue['explanation'][:64]
        ))

        for f in issue['affectedFiles']:
            file_id = get_file_id(scan_id, f['path'])
            affected_lines = json.dumps(f['lines'])
            # rel_issues_files columns: scan, issue, file, affectedLines
            sql = 'INSERT INTO rel_issues_files VALUES (%s, %s, %s, %s)'
            c.execute(sql, (scan_id, count, file_id, affected_lines))

        d.commit()
        count += 1

def query_issues (scan_id):
    sql = 'SELECT id, type FROM issues WHERE scan = %s'
    rows = query(sql, scan_id)
    items = []
    for row in rows:
        items.append({
            'id': row[0],
            'type': row[1]
        })
    return items

def get_issue (scan_id, issue_id):
    sql = 'SELECT scanner, rule, info, cli, type, explanation FROM issues ' + \
            'WHERE scan = %s AND id = %s'
    rows = query(sql, scan_id, issue_id)
    if not rows:
        return None
    issue = {
        'source': {
            'scanner': rows[0][0],
            'rule': rows[0][1],
            'info': rows[0][2],
            'cli': rows[0][3]
        },
        'type': rows[0][4],
        'explanation': rows[0][5],
    }

    sql = 'SELECT path, affectedLines FROM ' + \
            'rel_issues_files AS r JOIN files AS f ' + \
            'ON r.scan = f.scan AND r.file = f.id ' + \
            'WHERE r.scan = %s AND r.issue = %s'
    rows = query(sql, scan_id, issue_id)
    affectedFiles = []
    for row in rows:
        affectedFiles.append({
            'path': row[0],
            'lines': json.loads(row[1])
        })
    issue['affectedFiles'] = affectedFiles
    return issue

def getScanId (scan_uuid, apikey):
    sql = 'SELECT id FROM scans AS s JOIN rel_apikeys_scans AS r ' + \
            'ON s.id = r.scan WHERE apikey = %s AND uuid = uuid_to_bin(%s)'
    rows = query(sql, apikey, scan_uuid)
    if not rows:
        return None
    return rows[0][0]

def queryFiles (scan_id):
    sql = 'SELECT path FROM files WHERE scan = %s'
    rows = query(sql, scan_id)
    items = []
    for row in rows:
        items.append(row[0])
    return items

def getFile (scan_id, path, blob_base):
    sql = 'SELECT path, contentType, size FROM files WHERE ' + \
            'scan = %s AND path = %s'
    rows = query(sql, scan_id, path)
    if not rows:
        return False
    return {
        'path': rows[0][0],
        'type': rows[0][1],
        'size': rows[0][2],
        'blob': blob_base + '/' + rows[0][0],
    }
