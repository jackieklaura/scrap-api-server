# SCRAP API Server [:construction: WIP! :construction:]

The _**S**ecure **C**ode **R**eview **A**utomated **P**latform_ currently is
a prototype developed in context of my master thesis at the
[FH Technikum Wien](https://www.technikum-wien.at/). The main idea was to
test, whether it is feasible to use F/LOSS static code analysis tools
to create a toolchain/platform that evaluates computer science student's
code submissions for potential vulnerabilities and provides feedback on
how to improve in terms of secure coding. For more details on the whole
project visit the project site at https://scrap.tantemalkah.at

This API server is built with the python-flask and flask-restful framework
and currently facilitates two scanners:

- [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) using the [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit) ruleset.
- [YARA](https://virustotal.github.io/yara/) with rule set of [PHP Malware Finder](https://github.com/jvoisin/php-malware-finder) plus additional rules added by me

Prior to the implementation of the prototype the API was developed with the
[swagger.io](https://swagger.io) tools as an OpenAPI 3.0 specification. You
can check out the [published API on SwaggerHub](https://app.swaggerhub.com/apis/tantemalkah/SCRAP/1.0.0).
There is also a a [static version on my project site](https://scrap.tantemalkah.at/api-doc/)
generated from the SwaggerHub export. The plain specification resides in the
[scrap_api.yaml](static/scrap_api.yaml) file in this repo. These are currently
synchronized manually. In case of doubt, treat the version on SwaggerHub as the origin.

## Setup

Prerequisites for running either a local development instance or a (evaluative)
productive server, are a MariaDB/MySQL database and functioning setups of
PHP_CodeSniffer and YARA with our custom rulesets. Work through these sections
first, before setting up a local dev or a (public) production instance.

This setup was tested in the following environments:

- **Prod**: Debian 10 minimal with Apache 2.4.38, MariaDB 10.3.22, and Python 3.7.3
- **Dev**: Ubuntu 18.04 with MariaDB 10.1.44 and Python 3.6.9

All further explanations assume that also on Debian the `sudo` package is
installed and the user executing the commands is in the _sudo_ group.


### PHP_CodeSniffer

To run PHP_CodeSniffer you only need a PHP interpreter and the phpcs.phar
file. For more details check out the [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
repository, and the [scrap-scanner-eval](https://gitlab.com/jackieklaura/scrap-scanner-eval)
repository, that provides some background on how we use it in SCRAP.

The most basic setup is as follows:

```bash
sudo mkdir -p /opt/scrap/scanners/phpcs
cd /opt/scrap/scanners/phpcs
wget https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
```

As SCRAP does not use PHP_CodeSniffers's own rule set, but rather the security
focused [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit),
we additionally need this:

```bash
cd /opt/scrap/scanners
git clone https://github.com/FloeDesignTechnologies/phpcs-security-audit.git
ln -s phpcs-security-audit phpcs-sa
```


### YARA

Depending on whether your set YARA up for a server deployment or your
local dev environment, and in the latter case depending on your OS, there
a different prerequisites to set up YARA and its Python bindings.

In case you set it up for the production server as described below, the _yara_
package and the Python bindings are already installed through _apt_. In this
case you only need to set up the rules in _/opt/scrap/scanners/yara_, as described
below.

In case of a local dev environment, everything should be handled by _pip_ in
the virtual environment, see the section below.

In all other cases, check out the [YARA documentation](https://yara.readthedocs.io)
and the [yara-python](https://github.com/VirusTotal/yara-python) repository
for how to get the _yara_ module running.

Additionally to a funcational _yara_ module for Python, you need the SCRAP
rule set, consisting of the [PMF](https://github.com/jvoisin/php-malware-finder)
rule set plus own extensions (see the [scrap-scanner-eval](https://gitlab.com/jackieklaura/scrap-scanner-eval)
repo if you want to know more about the evaluation of PHP Malware Finder).
Therefore do the following:

```bash
sudo mkdir -p /opt/scrap/scanners/yara
cd /opt/scrap/scanners/
git clone https://github.com/jvoisin/php-malware-finder.git
cd yara
ln -s /opt/scrap/scanners/php-malware-finder/php-malware-finder/php.yar pmf.yar
ln -s /opt/scrap/scanners/php-malware-finder/php-malware-finder/whitelist.yar
ln -s /opt/scrap/scanners/php-malware-finder/php-malware-finder/whitelists
```

Then create a _scrap.yar_ file in the _/opt/scrap/scanners/yara_ directory
with the following contents:

```
include "pmf.yar"

rule SQLi
{
  meta:
    issue = "Your code might be vulnerable to an SQL injection"
    reason = "The $id parameter seems to not be sanitized"
    info = "https://scrap/description/sqli"

  strings:
    $unsanitized = /\$id\s*=\s*\$_REQUEST\[\s*['"]id['"]\s*\]\s*;/ nocase
    $injection = /SELECT.*FROM.*WHERE.*=\s*'\$id'/ nocase

  condition:
    $unsanitized and $injection
}
```

Additionally you can add your own yara rules in this file, or create another
file and use the `include` statement similar to how it is used to include
the _pmf.yar_ rule set.


### Database

To set up the database, on Debian as well as Ubuntu you can simply use _apt_:

```bash
sudo apt install mariadb-server
```

If you rather want to install MySQL instead of MariaDB, keep in mind the note at
the end of the section.

Once the database is installed, we have to create the databse for scrap. Connect
with `sudo mysql` and then use the following:

```sql
CREATE DATABASE scrap;
GRANT ALL PRIVILEGES ON scrap.* TO scrap@localhost IDENTIFIED BY 'yourdatabasepassword';
```

I suggest creating the database password with something like `pwgen -s 16 1`.
Whatever you choose as your password, you will have to use it in the _config.py_
file (see next section).

The final thing needed, is to populate the new database with the tables from
the _db\_init.sql_ file in _scrap-api-server_ repository. You can do this as
soon as you have used _git_ to clone it (in the descriptions for local or
remote setups below), with the following command, if you are inside the
root directory of the repository:

```bash
mysql -u scrap -p scrap < db_init.sql
```

__Note:__ Be aware that we in the [db_init.sql](db_init.sql) database scheme, we
are creating the functions `bin_to_uuid` and `uuid_to_bin` to transform between
human-readable string representations and the more effiction binary form of
UUIDs, as these functions are (not yet) available in MariaDB and also MySQL
prior to version 8. I did not test if the setup still works with MySQL 8 where
these functions are already available natively.


### SCRAP config

The main configuration of the SCRAP server happens in the _config.py_ file,
which first has to be created from the template in _config.sample.py_:

```bash
cp config.sample.py config.py
```

The comments in the file should be verbose enough to tell you what to change.
In any case it is important to also create the two directories, which are set in
`Config["uploads"]["folder"]` and `Config["uploads"]["temp_folder"]`. In a
standard setup where you don't change these values from the default, you just
have to do this inside the SCRAP server root:

```bash
mkdir uploads uploads_temp
```

If you allow for public users and change the `Config["auth"]["public_user_can_delete"]`
setting to `False`, make sure to create a cron job or other method of periodic
cleanup of public scans and also change the `Config["auth"]["public_user_cleanup_schedule"]`
setting accordingly (at least if you deploy to a publicly accessible server).


### Local dev setup

The following description also assumes that you have already set up the scanners
as described in the sections above.

If you have not installed _virtualenv_ and _pip3_ yet, do the following:

```bash
sudo apt install python3-pip virtualenv
```

Then, wherever you want to have the repo, clone it with:

```bash
git clone https://gitlab.com/jackieklaura/scrap-api-server.git
```

Now create the virtual environment and install all dependencies:

```bash
cd scrap-api-server
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
```

Then create a _config.py_ file as described above in the _SCRAP config_ section.
And set up the database as described above in the _Database_ section.

You are good to go, start up the development server with:

```bash
python scrap.py
```


### Deploying to a public/production server

The general documentation on deploying a Flask application can be found
on https://flask.palletsprojects.com/en/1.1.x/deploying

Here I describe a particular setup on a Debian 10 system, using Apache 2 as
a web server. I also use the [certbot](https://certbot.eff.org/) to obtain
a [Let's Encrypt](https://letsencrypt.org/) TLS certificate for the server,
to make the services available only on HTTPS. This assumes that you have a
valid public DNS bound to the IP of your server.

The following description also assumes that your basic Debian 10 installation
already has the `sudo` package installed and the user executing the commands
is in the _sudo_ group, and that you have set up the scanners as described
in the sections above.

```bash
# for the basic web and database server:
sudo apt install apache2 mariadb-server certbot libapache2-mod-wsgi-py3
# if you have git not yet installed:
sudo apt install git
# for the scanners we need:
sudo apt install yara php php-tokenizer
# for python and its dependency management:
sudo apt install python3-pip python3-yara
# install the python module dependencies
sudo pip3 install Flask Flask-RESTful Flask-Cors
sudo pip3 install Flask Flask-RESTful Flask-Cors
sudo pip3 install mysql-connector PyYAML==5.3.1 pytz
```

Now let's create a web root and clone the repo:

```bash
cd /var/www
sudo mkdir scrap-api-server
sudo git clone https://gitlab.com/jackieklaura/scrap-api-server.git
```

And create a specific user the web service should be run as:

```bash
sudo adduser --no-create-home --home /var/www/scrap-api-server --disabled-password --shell /bin/false www-scrap-api-server
sudo chown -R www-scrap-api-server:jackie scrap-api-server
```

Then create a _config.py_ file as described above in the _SCRAP config_ section.
And set up the database as described above in the _Database_ section.

Before you actually start the production server you can test, if the application
is working and no module or other thing is missing:

```bash
sudo -u www-scrap-api-server python3 scrap.py
```

Stop the test development server with CTRL-C again if everything is fine.
You can use `curl localhost:5000` and it should return the web services
meta information as a JSON object.

Now the three things left are:

1. a TLS certificate
2. a WSGI file
3. an Apache virtual host configuration to host the WSGI service

For the first we use `certbot`, granted that your server is reachable through
the according domain name, that you should change in the following command to
whatever you have (Apache should be hosting its default page, so that should
just work):

```bash
certbot certonly --webroot -w /var/www/html -d scrap.tantemalkah.at --dry-run
# only if the dry-run is successful do the same again without the --dry-run argument
certbot certonly --webroot -w /var/www/html -d scrap.tantemalkah.at
```

Now that we have a certificate let's set up the _scrap.wsgi_ file in the
_/var/www/scrap-api-server_ directory with the following content:

```python
import sys
sys.path.insert(0, '/var/www/scrap-api-server')
from scrap import app as application
```

The last thing now is the Apache virtual host configuration, which we put into
_/etc/apache2/sites-available/scrap.tantemalkah.at.conf_ with the following
content and replace all the occurances of `scrap.tantemalkah.at` with whatever
domain name you are using, and the `ServerAdmin` with your contact:

```apache2
<VirtualHost *:80>
    ServerName scrap.tantemalkah.at
    ServerAdmin change.this@scrap.tantemalkah.at

    RedirectPermanent / https://scrap.tantemalkah.at/
</VirtualHost>

<VirtualHost *:443>
    ServerName scrap.tantemalkah.at
    ServerAdmin change.this@scrap.tantemalkah.at

    DocumentRoot /var/www/html
    ErrorLog /var/log/apache2/scrap.tantemalkah.at-error.log
    TransferLog /var/log/apache2/scrap.tantemalkah.at-access.log

    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/scrap.tantemalkah.at/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/scrap.tantemalkah.at/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/scrap.tantemalkah.at/chain.pem

    WSGIDaemonProcess scrapplication user=www-scrap-api-server group=www-scrap-api-server threads=5
    WSGIScriptAlias /api/v1 /var/www/scrap-api-server/scrap.wsgi
    <Directory /var/www/scrap-api-server>
        WSGIProcessGroup scrapplication
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
    </Directory>
</VirtualHost>
```

Now we just need to activate Apache's _ssl_ module, disable the default site and
enable our new site:

```bash
sudo a2enmod ssl
sudo a2dissite 000-default.conf
sudo a2ensite scrap.tantemalkah.at.conf
```

Test the config and reload the Apache config:
```bash
sudo apachectl configtest
# only do the following if the test was successful
sudo apachectl graceful
```

If everything went fine, you should be able to access the SCRAP API at
https://scrap.tantemalkah.at/api/v1/ (scrap.tantemalkah.at substituted by
your domain name).


**Further suggestions:**

- Install a basic _postfix_ to be able to retrieve monitoring mails
- Install _fail2ban_ to block malicious attempts to test your system
