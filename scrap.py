from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from config import Config as cfg
import db
from resources.index import Index
from resources.scan import Scan
from resources.scan import ListOfScans
from resources.files import File
from resources.files import FileBlob
from resources.files import ListOfFiles
from resources.issues import Issue
from resources.issues import ListOfIssues
from resources.explanation import Explanation
from resources.explanation import ListOfExplanations
from resources.scanner import *
import uuid

app = Flask(__name__)
db.init_app(app)
api = Api(app)
CORS(app)

app.config['SCANNERS'] = [
    PHP_CodeSniffer(),
    YARAScanner(),
]
app.config['UPLOADS'] = cfg['uploads']
app.config['MAX_CONTENT_LENGTH'] = cfg['uploads']['size_limit']
app.config['PUBLICUSER'] = cfg['auth']['public_user']
app.config['PUBLICDELETE'] = cfg['auth']['public_user_can_delete']
app.config['PUBLICCLEANUP'] = cfg['auth']['public_user_cleanup_schedule']

api.add_resource(Index, '/')
api.add_resource(ListOfScans, '/scans')
api.add_resource(Scan, '/scans/<string:id>')
api.add_resource(ListOfFiles, '/scans/<string:scanid>/files')
api.add_resource(Issue, '/scans/<string:scan_uuid>/issues/<int:issue_id>')
api.add_resource(ListOfIssues, '/scans/<string:scan_uuid>/issues')
api.add_resource(File, '/scans/<string:scanid>/files/<path:path>')
api.add_resource(FileBlob, '/scans/<string:scanid>/blob/<path:path>')
api.add_resource(ListOfScanners, '/scanners')
api.add_resource(ListOfExplanations, '/explanations')
api.add_resource(Explanation, '/explanations/<string:slug>')

if __name__ == '__main__':
    app.run(debug=True)
