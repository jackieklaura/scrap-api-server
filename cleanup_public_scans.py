from config import Config as cfg
import mysql.connector
import shutil
import os

db = mysql.connector.connect(
    host = cfg['db']['host'],
    user = cfg['db']['user'],
    passwd = cfg['db']['pass'],
    database = cfg['db']['database'],
)

def query(q, *args):
    """Make a generic query against the database and return all result rows."""
    c = db.cursor()
    c.execute(q, args)
    rows = c.fetchall()
    return rows

def deleteScan (uuid, apikey):
    """Delete a scan and return True on success, or False if no scan was found"""
    sql = 'SELECT id FROM scans AS s JOIN rel_apikeys_scans AS r ON s.id = r.scan ' + \
    'WHERE apikey = %s AND uuid = uuid_to_bin(%s)'
    c = db.cursor()
    c.execute(sql, (apikey, uuid))
    row = c.fetchone()
    if not row:
        return False
    id = row[0]
    sql = 'DELETE FROM rel_apikeys_scans WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM rel_issues_files WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM issues WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM files WHERE scan = %s'
    c.execute(sql, (id,))
    sql = 'DELETE FROM scans WHERE id = %s'
    c.execute(sql, (id,))
    db.commit()
    shutil.rmtree(os.path.join(cfg['uploads']['folder'], uuid))
    return True

sql = 'SELECT id FROM apikeys WHERE user = %s'
rows = query(sql, cfg['auth']['public_user'])
apikey = rows[0][0]

sql = 'SELECT bin_to_uuid(uuid) FROM scans AS s ' + \
        'JOIN rel_apikeys_scans AS r ON s.id = r.scan ' +\
        'WHERE apikey = %s'
rows = query(sql, apikey)

if len(rows):
    print(str(len(rows)) + ' public scans found. Cleaning up.')
    for row in rows:
        print('Deleting scan ' + str(row[0]))
        deleteScan(row[0], apikey)
    print('Done. No public scans left.')
else:
    print('No public scans found.')
