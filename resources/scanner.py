from flask_restful import Resource
from flask import g, current_app
import db
import yara
import json
import os
import subprocess
from subprocess import PIPE

class ListOfScanners(Resource):
    def get(self):
        response = []
        for scanner in current_app.config['SCANNERS']:
            response.append(scanner.getMeta())
        return response

class Scanner():
    def __init__(self, name, slug=None, version=None, uri=None, comment=None):
        self.name = name
        self.slug = slug
        self.version = version
        self.uri = uri
        self.comment = comment
        self.issues = []

    def __repr__(self):
        return '<Scanner ' + self.slug + ' ("' + self.name + '")'

    def getMeta(self):
        return {
            'name': self.name,
            'slug': self.slug,
            'version': self.version,
            'uri' : self.uri,
            'comment': self.comment,
        }

    def submit_to_db(self, uuid):
        db.submit_issues(uuid, self.issues)

class PHP_CodeSniffer(Scanner):
    def __init__(self):
        super().__init__(
            name='PHP_CodeSniffer',
            slug='phpcs',
            version='3.5.4',
            uri='https://github.com/squizlabs/PHP_CodeSniffer',
            comment='Using the [phpcs-security-audit v2](https://github.com/FloeDesignTechnologies/phpcs-security-audit)',
        )
        self.cli_pattern='php /opt/scrap/scanners/phpcs/phpcs.phar ' +\
            '--standard=/opt/scrap/scanners/phpcs-sa/Security ' +\
            '-s --report=json %s'

    def scan_folder(self, folder):
        self.issues = []
        p = subprocess.run([
            'php', '/opt/scrap/scanners/phpcs/phpcs.phar',
            '--standard=/opt/scrap/scanners/phpcs-sa/Security',
            '-s', '--report=json', folder
        ], stdout=PIPE, stderr=PIPE)
        findings = json.loads(p.stdout)

        for file, finding in findings['files'].items():
            path = file.rsplit(folder, 1)[1]
            if path[0] == '/':
                path = path[1:]
            for issue in finding['messages']:
                self.issues.append({
                    'source': {
                        'scanner': self.slug,
                        'rule': issue['source'],
                        'info': self.uri,
                        'cli': self.cli_pattern.replace('%s', path)
                    },
                    'type': issue['source'],
                    'explanation': 'phpcs.' + issue['source'],
                    'affectedFiles': [
                        {
                            'path': path,
                            'lines': [
                                {
                                    'num': issue['line'],
                                    'characters': {
                                        'from': issue['column']
                                    },
                                    'text': '',
                                    'description': issue['type'] + ' | ' + \
                                        'severity: ' + str(issue['severity']) + \
                                        ' | ' + issue['message']
                                }
                            ]
                        }
                    ]
                })
        return self.issues

class YARAScanner(Scanner):
    def __init__(self):
        super().__init__(
            name='YARA',
            slug='yara',
            version='3.7.1',
            uri='https://virustotal.github.io/yara/',
            comment='Including the rule set of [PHP Malware Finder](https://github.com/jvoisin/php-malware-finder)',
        )
        self.cli_pattern='yara -r -w -s -m /opt/scrap/scanners/yara/scrap.yar %s'
        self.rules = yara.compile(filepath='/opt/scrap/scanners/yara/scrap.yar')

    def scan_folder(self, folder):
        self.issues = []
        for dir in os.walk(folder):
            for file in dir[2]:
                matches = self.rules.match(dir[0]+'/'+file)
                for m in matches:
                    issue = {
                        'source': {
                            'scanner': self.slug,
                            'rule': m.rule,
                            'info': self.uri,
                            'cli': self.cli_pattern.replace('%s', dir[0]+'/'+file, 1)
                        },
                        'type': m.rule,
                        'explanation': 'yara.' + m.rule,
                        'affectedFiles': [
                            {
                                'path': str(dir[0]+'/'+file)[len(folder)+1:],
                                'lines': [
                                ]
                            },
                        ]
                    }
                    description = ''
                    if m.meta:
                        if 'issue' in m.meta:
                            description += m.meta['issue'] + ' | '
                        if 'reason' in m.meta:
                            description += m.meta['reason'] + ' | '
                        if 'info' in m.meta:
                            description += 'More info at: ' + m.meta['info']
                    for s in m.strings:
                        issue['affectedFiles'][0]['lines'].append({
                            'characters': {
                                'from': s[0],
                                'to': s[0] + len(s[2])
                            },
                            'text': str(s[2]),
                            'description': description,
                        })
                    self.issues.append(issue)
        return self.issues
