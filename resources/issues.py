from flask_restful import Resource
from flask import g, current_app, request
import db
from common import sanitize
from common import auth
from common import default_responses as resp

class ListOfIssues(Resource):
    def get(self, scan_uuid):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        apikey = request.headers.get('X-API-KEY')
        if not sanitize.isUuid(scan_uuid):
            return resp.InvalidUuid
        scan_id = db.getScanId(scan_uuid, apikey)
        if scan_id == None:
            return resp.ScanNotFound
        items = db.query_issues(scan_id)
        response = {
            'paging': {
                'count': len(items),
                'next': '',
                'previous': '',
            },
            'items': items
        }
        return response

class Issue(Resource):
    def get(self, scan_uuid, issue_id):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        apikey = request.headers.get('X-API-KEY')
        if not sanitize.isUuid(scan_uuid):
            return resp.InvalidUuid
        scan_id = db.getScanId(scan_uuid, apikey)
        if scan_id == None:
            return resp.ScanNotFound
        issue = db.get_issue(scan_id, issue_id)
        return issue
