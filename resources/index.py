from flask_restful import Resource
from flask import url_for

class Index(Resource):
    def get(self):
        return {
            'api': 'scrap',
            'version': '1.0.0',
            'openapi_file': url_for('static', filename='scrap_api.yaml'),
            'definition': 'https://app.swaggerhub.com/apis/tantemalkah/SCRAP/1.0.0',
            'documentation': 'https://scrap.tantemalkah.at',
        }
