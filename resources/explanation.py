from flask_restful import Resource
from flask import current_app
from common import default_responses as resp
import yaml
import os
import re

class Explanation(Resource):
    def get(self, slug):
        if not os.path.exists(os.path.dirname(__file__) + '/explanations/' + slug + '.yaml'):
            return resp.ExplanationNotFound
        with open(os.path.dirname(__file__) + '/explanations/' + slug + '.yaml', 'r') as f:
            try:
                explanation = yaml.load(f, Loader=yaml.FullLoader)
            except:
                return resp.ExplanationParseError
        needed = {'isStub', 'shortDescription', 'name', 'type'}
        if (not needed <= set(explanation.keys())):
            return resp.ExplanationParamMissing
        return explanation

class ListOfExplanations(Resource):
    def get(self):
        files = os.scandir(os.path.dirname(__file__) + '/explanations')
        needed = {'isStub', 'shortDescription', 'name', 'type'}
        items = []
        for file in files:
            if not file.is_file():
                continue
            if not re.match(r'^.*\.yaml$', file.name):
                continue
            with open(file.path, 'r') as f:
                try:
                    explanation = yaml.load(f, Loader=yaml.FullLoader)
                except:
                    continue
            if (not needed <= set(explanation.keys())):
                continue
            items.append({
                'name': explanation['name'],
                'type': explanation['type'],
                'slug': file.name[:-5], # cut away the .yaml part
            })

        response = {
            'paging': {
                'count': len(items),
                'next': '',
                'previous': '',
            },
            'items': items
        }
        return response
