from flask_restful import Resource
from flask import g, current_app, request
from werkzeug.utils import secure_filename
import db
from common import sanitize
from common import auth
from common import default_responses as resp
from resources import scanner
import tarfile
import uuid
import json
import re
import os

class ListOfScans(Resource):
    def get(self):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        items = db.queryScans(request.headers.get('X-API-KEY'))
        response = {
            'paging': {
                'count': len(items),
                'next': '',
                'previous': '',
            },
            'items': items
        }
        return response

    def post(self):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not re.match(r'^multipart\/form-data(; boundary=[0-9a-fA-F_\-]*)?$', str(request.content_type)):
            return resp.UseMultipart
        if not 'file' in request.files:
            return resp.FileNeeded
        file = request.files['file']
        if not '.' in file.filename:
            return resp.WrongFileType
        suffix = file.filename.rsplit('.', 1)[1]
        if not suffix in current_app.config['UPLOADS']['allowed_types']:
            return resp.WrongFileType
        file.seek(0, os.SEEK_END)
        filesize = file.tell()
        file.seek(0, os.SEEK_SET)
        if request.headers.get('X-API-KEY') == current_app.config['PUBLICUSER'] \
            and filesize > current_app.config['UPLOADS']['public_size_limit']:
                return resp.FileTooBig
        if filesize == 0:
            return resp.NoEmptyFiles

        # after this first validation we store the file in a temporary folder
        # to do further checks on the file/archive
        dn_temp = os.path.join(
            current_app.config['UPLOADS']['temp_folder'],
            str(uuid.uuid1())  # if someone else submits the same filename while still processing this one
            )
        fn_temp = secure_filename(file.filename)
        os.mkdir(dn_temp)
        file.save(os.path.join(dn_temp, fn_temp))

        # if it is not a single file, but a tar archive we have to check for
        # potential path traversals
        if suffix == 'tgz':
            archive = tarfile.open(os.path.join(dn_temp, fn_temp), 'r:gz')
            contains_php = False
            for f in archive.getmembers():
                if f.name.startswith(('../', './', '/')):
                    return resp.InvalidArchiveContent
                if f.name.endswith('.php'):
                    contains_php = True
            if not contains_php:
                return resp.InvalidArchiveContent

        # ok, we are good. we can store/extract the files to their final
        # location. but need to generate the new scan in the DB first,
        # to get its UUID, which we use for the storage path
        scan = db.submitScan(request.headers.get('X-API-KEY'))
        scan_uuid = scan['id']
        # TODO for post-prototype stage:
        # implement multithreaded version; the response could be returned now
        # while the file indexing and the acutal scans can be started independently
        # to update the scan (and its progress) in the DB
        dn_final = os.path.join(current_app.config['UPLOADS']['folder'], scan_uuid)
        os.mkdir(dn_final)
        if suffix == 'php':
            os.rename(os.path.join(dn_temp, fn_temp), os.path.join(dn_final, fn_temp))
        elif suffix == 'tgz':
            archive.extractall(dn_final)

        # cleaning up the temporary folder
        if suffix == 'tgz':
            os.remove(os.path.join(dn_temp, fn_temp))
        os.rmdir(dn_temp)

        if not db.populateFiles(scan_uuid):
            return resp.InternalServerError

        issues = []
        if request.form.get('scanner'):
            if request.form['scanner'] == 'yara':
                scanners = [scanner.YARAScanner()]
            elif request.form['scanner'] == 'phpcs':
                scanners = [scanner.PHP_CodeSniffer()]
            else:
                return resp.InvalidScanner
        else:
            scanners = [scanner.YARAScanner(), scanner.PHP_CodeSniffer()]
        for s in scanners:
            s.scan_folder(dn_final)
            issues.extend(s.issues)
            s.submit_to_db(scan_uuid)

        files = db.get_file_count(scan_uuid)
        db.update_scan(scan_uuid, stage='done', percentage=100, \
                    issuesFound=len(issues), files=files, analysed='now')
        response = db.getScan(scan_uuid, request.headers.get('X-API-KEY'))
        if request.form.get('withIssues') and request.form['withIssues'] == 'true':
            response['issues'] = issues
        return response

class Scan(Resource):
    def get(self, id):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not sanitize.isUuid(id):
            return resp.InvalidUuid
        response = db.getScan(id, request.headers.get('X-API-KEY'))
        if not response:
            return resp.ScanNotFound
        return response

    def delete(self, id):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not sanitize.isUuid(id):
            return resp.InvalidUuid
        if request.headers.get('X-API-USER') == current_app.config['PUBLICUSER'] \
            and not current_app.config['PUBLICDELETE']:
                return resp.NoPublicDelete
        if not db.deleteScan(id, request.headers.get('X-API-KEY')):
            return resp.ScanNotFound
        return None, 204
