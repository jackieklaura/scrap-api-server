from flask_restful import Resource
from flask import g, current_app, request, send_file
import db
from common import sanitize
from common import auth
from common import default_responses as resp
import os

class ListOfFiles(Resource):
    def get(self, scanid):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not sanitize.isUuid(scanid):
            return resp.InvalidUuid
        id = db.getScanId(scanid, request.headers.get('X-API-KEY'))
        if id == None:
            return resp.ScanNotFound
        items = db.queryFiles(id)
        response = {
            'paging': {
                'count': len(items),
                'next': '',
                'previous': '',
            },
            'items': items
        }
        return response

class File(Resource):
    def get(self, scanid, path):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not sanitize.isUuid(scanid):
            return resp.InvalidUuid
        id = db.getScanId(scanid, request.headers.get('X-API-KEY'))
        if id == None:
            return resp.ScanNotFound
        # TODO: although we are safe here, because path is just used as a
        #       parameterised string to search the files table for, it would
        #       be better practice to generally use a sanitized version. so
        #       create a path function in the common.sanitize module!
        response = db.getFile(id, path, '/scans/'+scanid+'/blob')
        if not response:
            return resp.FileNotFound
        return response

class FileBlob(Resource):
    def get(self, scanid, path):
        if not auth.isUser(request.headers):
            return resp.NotAuthorized
        if not sanitize.isUuid(scanid):
            return resp.InvalidUuid
        id = db.getScanId(scanid, request.headers.get('X-API-KEY'))
        if id == None:
            return resp.ScanNotFound
        filemeta = db.getFile(id, path, '')
        if not filemeta:
            return resp.FileNotFound
        filename = os.path.join(
            current_app.config['UPLOADS']['folder'],
            scanid,
            filemeta['path']
        )
        return send_file(filename, filemeta['type'])
