To create a new template copy the file `_template.yaml.example` to a file
in the format `<slug>.yaml`, where `<slug>` can be choosen to reflect the
name of the explanation in a brief way. It will also be used by the API
as the identified for retrieving the explanation.

After copying the file, adapt all the properties and you should be able to
retrieve the explanation through the `/explanations/<slug>` endpoint. It also
should show up in the listing you get at the `/explanations` endpoint.

**Note** that only those explanations will be included in the listing, which
are valid YAML files and can be parsed. Also at least the following 4 fields
have to be set:

- name
- type
- isStub
- shortDescription

So the following explanation file would be valid and listed under the
`/explanations` endpoint:

```yaml
name: A random random error
type: bad crypto
isStub: true
shortDescription: Use of pseudo-random generators for cryptographic functions
```

The `isStub` property is used to flag explanations which are not yet complete
and should be extended. Only set it to _false_, if you at least provide
a `longDescription` and a `howToFix` property as well with some reasonable
expressive content.
