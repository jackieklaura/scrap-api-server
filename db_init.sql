DROP TABLE IF EXISTS rel_issues_files;
DROP TABLE IF EXISTS rel_apikeys_scans;
DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS issues;
DROP TABLE IF EXISTS scans;
DROP TABLE IF EXISTS apikeys;
DROP FUNCTION IF EXISTS uuid_to_bin;
DROP FUNCTION IF EXISTS bin_to_uuid;

CREATE TABLE scans (
  id INT AUTO_INCREMENT,
  uuid BINARY(16) UNIQUE,
  stage VARCHAR(10) NOT NULL,
  percentage INT DEFAULT 0,
  issuesFound INT DEFAULT 0,
  files INT,
  created DATETIME,
  analysed DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE issues (
  scan INT,
  id INT,
  scanner VARCHAR(64),
  rule TEXT,
  info TEXT,
  cli TEXT,
  type VARCHAR(64),
  explanation VARCHAR(64),
  FOREIGN KEY (scan) REFERENCES scans(id),
  CONSTRAINT pk_issues PRIMARY KEY (scan, id)
);

CREATE TABLE files (
  scan INT,
  id INT,
  path TEXT,
  contentType VARCHAR(128),
  size INT,
  FOREIGN KEY (scan) REFERENCES scans(id),
  CONSTRAINT pk_files PRIMARY KEY (scan, id)
);

CREATE TABLE rel_issues_files (
  scan INT,
  issue INT,
  file INT,
  affectedLines TEXT,
  CONSTRAINT fk_scan FOREIGN KEY (scan) REFERENCES scans(id),
  CONSTRAINT fk_issue FOREIGN KEY (scan, issue) REFERENCES issues(scan, id),
  CONSTRAINT fk_file FOREIGN KEY (scan, file) REFERENCES files(scan, id),
  CONSTRAINT pk_rel_issues_files PRIMARY KEY (scan, issue, file)
);

CREATE TABLE apikeys (
  id VARCHAR(64),
  user VARCHAR(64),
  comment TEXT,
  PRIMARY KEY (id)
);

CREATE TABLE rel_apikeys_scans (
  apikey VARCHAR(64),
  scan INT,
  CONSTRAINT fk_apikey FOREIGN KEY (apikey) REFERENCES apikeys(id),
  CONSTRAINT fk_scan2 FOREIGN KEY (scan) REFERENCES scans(id),
  CONSTRAINT pk_rel_apikeys_scans PRIMARY KEY (apikey, scan)
);

CREATE FUNCTION bin_to_uuid(uuid BINARY(16)) RETURNS VARCHAR(36)
RETURN LOWER(
  CONCAT(
    SUBSTR(HEX(uuid), 1, 8), '-',
    SUBSTR(HEX(uuid), 9, 4), '-',
    SUBSTR(HEX(uuid), 13, 4), '-',
    SUBSTR(HEX(uuid), 17, 4), '-',
    SUBSTR(HEX(uuid), 21)
  )
);

CREATE FUNCTION uuid_to_bin(uuid VARCHAR(36)) RETURNS BINARY(16)
RETURN UNHEX (
  REPLACE(uuid, '-', '')
);
